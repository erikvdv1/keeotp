@ECHO off
SET SOLUTIONDIR=%~dp0
SET PROJECTDIR=%SOLUTIONDIR%KeeOtp
SET KEEPASS=C:\Program Files (x86)\KeePass Password Safe 2\KeePass.exe

@RD /S /Q "%PROJECTDIR%\bin"
@RD /S /Q "%PROJECTDIR%\obj"
@RD /S /Q "%PROJECTDIR%\lib"
MKDIR "%PROJECTDIR%\lib"
COPY "%SOLUTIONDIR%packages\OtpSharp.1.0.5.0\lib\net40\OtpSharp.dll" "%PROJECTDIR%\lib\OtpSharp.dll"

"%KEEPASS%" --plgx-create "%PROJECTDIR%" --plgx-prereq-net:4.0